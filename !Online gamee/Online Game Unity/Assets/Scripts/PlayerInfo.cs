﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerInfo : NetworkBehaviour
{
    public NetworkManager_Custom netMCustom;

    [SyncVar(hook = "OnSyncMyNameHook")]
    public string playerName = "";

    [SyncVar (hook = "OnChangeHealth")]
    public float health = 100;

    public Image healthBar;

    public override void OnStartLocalPlayer()
    {
        netMCustom = GameObject.Find("Network Manager").GetComponent<NetworkManager_Custom>();
        netMCustom.myPlayer = gameObject;
        CmdSetMyName(netMCustom.playerName);
        transform.GetComponentInChildren<Text>().color = Color.red;
    }

    private void Start()
    {
        // because SyncVar hook is not fired on startup only for future changes?
        OnSyncMyNameHook(playerName);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isLocalPlayer)
            {
                CmdTakeDamage();
            }
        }
    }

    [Command]
    public void CmdTakeDamage()
    {
        health -= 10;
    }

    [Command]
    void CmdSetMyName(string name)
    {
       playerName = name;
    }

    public void OnSyncMyNameHook(string value) //change other players name
    {
       playerName = value;
       transform.GetComponentInChildren<Text>().text = value;
    }

    public void OnChangeHealth(float value)
    {
        healthBar.fillAmount = value / 100;
    }
}
