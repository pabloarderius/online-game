﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class PlayerControlR : NetworkBehaviour
{
    public float speed;
    public float turnSmoothing = 40;

    private float movH, movV;
    private Rigidbody rb;
    private Transform myCamera;
    private Animator anim;

    private void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        float movH = Input.GetAxis("Horizontal");
        float movV = Input.GetAxis("Vertical");
        if (movH != 0f || movV != 0f)
        {
            rb.velocity = new Vector3(movH * speed, rb.velocity.y, movV * speed);
            Rotating(movH, movV);
        }
        anim.SetFloat("Running", Mathf.Abs(movH) + Mathf.Abs(movV));
    }

    void Rotating(float horizontal, float vertical)
    {
        Vector3 forward = myCamera.TransformDirection(Vector3.forward);

        forward.y = 0.0f;
        forward = forward.normalized;

        Vector3 right = new Vector3(forward.z, 0, -forward.x);

        Vector3 targetDirection;

        float finalTurnSmoothing;

        targetDirection = forward * vertical + right * horizontal;
        finalTurnSmoothing = turnSmoothing;

        if ((targetDirection != Vector3.zero))
        {
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

            Quaternion newRotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, targetRotation, finalTurnSmoothing * Time.deltaTime);
            GetComponent<Rigidbody>().MoveRotation(newRotation);
        }
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        rb = GetComponent<Rigidbody>();
        myCamera = GetComponentInChildren<Camera>().transform;
        anim = GetComponentInChildren<Animator>();
      //  GetComponent<MeshRenderer>().material.color = Color.blue;
    }
}