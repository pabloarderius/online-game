﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkManager_Custom : NetworkManager {

    public Text inputFieldIpText;
    public Text inputFieldPlayerName;
    public string playerName;
    public GameObject myPlayer;

	public void StartupHost()
    {
        SetPort();
        NetworkManager.singleton.StartHost();
    }

    public void JoinGame()
    {
        SetIPAdress();
        SetPort();
        NetworkManager.singleton.StartClient();
    }

    private void SetPort()
    {
        NetworkManager.singleton.networkPort = 7777;
        SetPlayerName();
    }

    private void SetIPAdress()
    {
        string ipAddress = inputFieldIpText.text;
        NetworkManager.singleton.networkAddress = ipAddress;
    }

    private void SetPlayerName()
    {
        playerName = inputFieldPlayerName.text;
        Debug.Log(playerName);
    }

    //MatchMaking


}
