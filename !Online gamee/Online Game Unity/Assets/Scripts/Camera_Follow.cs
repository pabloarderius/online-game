﻿using UnityEngine;
using System.Collections;


public class Camera_Follow : MonoBehaviour {

	public Transform target;
	public float distance = 7.0f;
	public float height = 17f;
	public float heightDamping = 2.0f;
	public float rotationDamping = 0.0f;

    private void Start()
    {
        target = transform.parent;
        transform.parent = null;
    }

    void LateUpdate () {
        if (target.transform.GetComponent<PlayerControlR>().isLocalPlayer == false)
        {
            GetComponent<Camera>().gameObject.SetActive(false);
        }
        else
        {
            GetComponent<Camera>().gameObject.SetActive(true);
        }

        if (!target)
			return;

		float wantedRotationAngle = target.eulerAngles.y;
		float wantedHeight = target.position.y + height;
		
		float currentRotationAngle = transform.eulerAngles.y;
		float currentHeight = transform.position.y;
		
		currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
		
		currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);
		
		Quaternion currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
		
		transform.position = target.position;
		transform.position -= currentRotation * Vector3.forward * distance;
		Vector3 temp = transform.position;
		temp.y = currentHeight; 
		transform.position = temp;
		
		transform.LookAt (target);
	}
}