﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    private Rigidbody rb;
    public float rotSpeed = 5;
    public float moveSpeed = 10;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        float mouseX = Input.GetAxis("Mouse X");


        //rb.velocity = new Vector3(h, rb.velocity.y, Vector3.forward.z * v);

        Vector3 forward = transform.InverseTransformDirection(transform.position);
        //forward.y = 0.0f;
        //forward = forward.normalized;

        float speedDelta = moveSpeed * Time.deltaTime;

        Vector3 right = new Vector3(forward.x*h* speedDelta, 0, forward.z*v* speedDelta);

        rb.velocity = right;


        transform.Rotate(transform.eulerAngles.x, mouseX * rotSpeed * Time.deltaTime, 0);


    }
}
