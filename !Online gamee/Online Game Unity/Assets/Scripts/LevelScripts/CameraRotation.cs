﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

    public Transform target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(target);

        float mouseY = Input.GetAxis("Mouse Y");
        transform.parent.Rotate(mouseY, transform.eulerAngles.y, 0);

    }
}
