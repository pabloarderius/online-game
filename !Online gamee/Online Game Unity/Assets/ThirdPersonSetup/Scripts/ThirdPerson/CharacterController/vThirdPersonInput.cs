﻿using UnityEngine;
using UnityEngine.Networking;
#if UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif

namespace Invector.CharacterController
{
    public class vThirdPersonInput : NetworkBehaviour
    {
        #region variables

        [Header("Default Inputs")]
        public string horizontalInput = "Horizontal";
        public string verticallInput = "Vertical";
        public KeyCode jumpInput = KeyCode.Space;
        public KeyCode strafeInput = KeyCode.Tab;
        public KeyCode sprintInput = KeyCode.LeftShift;

        [Header("Camera Settings")]
        public string rotateCameraXInput ="Mouse X";
        public string rotateCameraYInput = "Mouse Y";

        protected vThirdPersonCamera tpCamera;                // acess camera info        
        [HideInInspector]
        public string customCameraState;                    // generic string to change the CameraState        
        [HideInInspector]
        public string customlookAtPoint;                    // generic string to change the CameraPoint of the Fixed Point Mode        
        [HideInInspector]
        public bool changeCameraState;                      // generic bool to change the CameraState        
        [HideInInspector]
        public bool smoothCameraState;                      // generic bool to know if the state will change with or without lerp  
        [HideInInspector]
        public bool keepDirection;                          // keep the current direction in case you change the cameraState

        protected vThirdPersonController cc;                // access the ThirdPersonController component    
        public Camera cam;

        #endregion

        #region shootVariables
        public GameObject bullet;
        public Transform spawnBullet;

        #endregion

        public override void OnStartLocalPlayer()
        {

          //  cam.transform.parent = null;
        }


        protected virtual void Start()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            CharacterInit();
        }

        protected virtual void CharacterInit()
        {
            cc = GetComponent<vThirdPersonController>();
            if (cc != null)
                cc.Init();

            tpCamera = FindObjectOfType<vThirdPersonCamera>();
            if (tpCamera) tpCamera.SetMainTarget(this.transform);

            Cursor.visible = false;
        }

        protected virtual void LateUpdate()
        {
            if (cam == null)
            {
                cam = GetComponentInChildren<Camera>();
            }

            if (!isLocalPlayer)
            {
                return;
            }
            //else
            //{
            //    cam.gameObject.SetActive(true);
            //    cam.transform.parent = null;
            //}

            if (cc == null) return;             // returns if didn't find the controller		    
            InputHandle();                      // update input methods
            UpdateCameraStates();               // update camera states
        }

        protected virtual void FixedUpdate()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            cc.AirControl();
            CameraInput();
        }

        protected virtual void Update()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            cc.UpdateMotor();                   // call ThirdPersonMotor methods               
            cc.UpdateAnimator();                // call ThirdPersonAnimator methods		               
        }

        protected virtual void InputHandle()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            ExitGameInput();
            CameraInput();

            if (!cc.lockMovement)
            {
                MoveCharacter();
                SprintInput();
                StrafeInput();
                JumpInput();
                AimInput();
                Fire();
            }
        }

        #region Basic Locomotion Inputs      

        protected virtual void MoveCharacter()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            cc.input.x = Input.GetAxis(horizontalInput);
            cc.input.y = Input.GetAxis(verticallInput);

            if (PauseMenu.paused)
            {
                cc.input.x = 0;
                cc.input.y = 0;
            }    
        }

        protected virtual void StrafeInput()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            if (Input.GetKeyDown(strafeInput))
                cc.Strafe();
        }

        protected virtual void SprintInput()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            if (Input.GetKeyDown(sprintInput))
                cc.Sprint(true);
            else if(Input.GetKeyUp(sprintInput))
                cc.Sprint(false);
        }

        protected virtual void JumpInput()
        {
            if (!isLocalPlayer || PauseMenu.paused)
            {
                return;
            }
            if (Input.GetKeyDown(jumpInput))
                cc.Jump();
        }

        protected virtual void AimInput()
        {
            if (!isLocalPlayer || PauseMenu.paused)
            {
                return;
            }
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                cc.Aim();
            }
            if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                cc.Aim();
            }
        }

        void Fire()
        {
            if (!isLocalPlayer || PauseMenu.paused)
            {
                return;
            }
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                CmdFire();
            }
        }

        [Command]
        private void CmdFire()
        {
            GameObject rocket = Instantiate(bullet, spawnBullet.position, spawnBullet.rotation);
            rocket.GetComponent<Rigidbody>().velocity = rocket.transform.forward * 6.0f;
            //Spawn the bullet on the Clients
            NetworkServer.Spawn(rocket);
            Destroy(rocket, 5);
        }

        protected virtual void ExitGameInput()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            // just a example to quit the application 
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!Cursor.visible)
                    Cursor.visible = true;
            }
        }

        #endregion

        #region Camera Methods

        protected virtual void CameraInput()
        {
            if (!isLocalPlayer || PauseMenu.paused)
            {
                return;
            }

            if (tpCamera == null)
                return;
            var Y = Input.GetAxis(rotateCameraYInput);
            var X = Input.GetAxis(rotateCameraXInput);

            tpCamera.RotateCamera(X, Y);

            // tranform Character direction from camera if not KeepDirection
            if (!keepDirection)
                cc.UpdateTargetDirection(tpCamera != null ? tpCamera.transform : null);
            // rotate the character with the camera while strafing        
            RotateWithCamera(tpCamera != null ? tpCamera.transform : null);            
        }

        protected virtual void UpdateCameraStates()
        {
            if (!isLocalPlayer)
            {
                return;
            }
            // CAMERA STATE - you can change the CameraState here, the bool means if you want lerp of not, make sure to use the same CameraState String that you named on TPCameraListData
            if (tpCamera == null)
            {
                tpCamera = FindObjectOfType<vThirdPersonCamera>();
                if (tpCamera == null)
                    return;
                if (tpCamera)
                {
                    tpCamera.SetMainTarget(this.transform);
                    tpCamera.Init();
                }
            }            
        }

        protected virtual void RotateWithCamera(Transform cameraTransform)
        {
            if (!isLocalPlayer)
            {
                return;
            }
            if (cc.isStrafing && !cc.lockMovement && !cc.lockMovement)
            {                
                cc.RotateWithAnotherTransform(cameraTransform);                
            }
        }

        #endregion     
    }
}